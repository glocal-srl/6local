//
//  TotViewController.h
//  6local
//
//  Created by GLOCAL on 24/06/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TotViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *numeroTessera;
@property (weak, nonatomic) IBOutlet UILabel *punti;
@property (retain, nonatomic) NSURLConnection *connection;
@property (retain, nonatomic) NSMutableData *receivedData;

@end
