//
//  HomeViewController.m
//  6local
//
//  Created by GLOCAL on 12/06/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (IBAction)logout:(id)sender {
    
    //cancello dati login
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"json"];
    [[NSUserDefaults standardUserDefaults]synchronize ];
    
    //e rimando all'inizio
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"ViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    
    
    
    
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    
    //visualizzo le info sull'utente logatto
    NSString *jsonString = [[NSUserDefaults standardUserDefaults] objectForKey:@"json"];
    //NSString *jsonString =    @"{\"name\" : \"Dusty\", \"breed\": \"Poodle\", \"age\": \"7\"}";
    
    
    NSData* data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    //data = [data subdataWithRange:NSMakeRange(0, [data length] - 1)];
    
    NSError *e;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data
                                                                options:kNilOptions
                                                                  error:&e];
    NSString* nome = [dic objectForKey:@"denominazione"];
        
        
    _infoUtenteLabel.text = nome;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)leggiTessera:(id)sender {
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"CameraViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
