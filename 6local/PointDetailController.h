//
//  PointDetailController.h
//  6Local
//
//  Created by pierangelo on 20/05/15.
//  Copyright (c) 2015 Glocal srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface PointDetailController : UIViewController<MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;



@property (nonatomic, strong) NSString *myObject;

@property (weak, nonatomic) IBOutlet UINavigationItem *navigazio;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollVista;


@property (weak, nonatomic) IBOutlet UILabel *denominazioneLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoriaLabel;

@property (weak, nonatomic) IBOutlet UILabel *comuneLabel;
@property (weak, nonatomic) IBOutlet UILabel *indirizzoLabel;
@property (weak, nonatomic) IBOutlet UILabel *telefonoLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *webLabel;
@property (weak, nonatomic) IBOutlet UIButton *apri;
@property (weak, nonatomic) IBOutlet UIButton *chiudi;
@property (weak, nonatomic) IBOutlet UILabel *filetto;


@property (weak, nonatomic) IBOutlet UILabel *latitudineLabel;
@property (weak, nonatomic) IBOutlet UILabel *longitudineLabel;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;


@end
