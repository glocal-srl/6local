//
//  OperazioneViewController.m
//  6local
//
//  Created by GLOCAL on 18/06/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import "OperazioneViewController.h"

@interface OperazioneViewController ()

@end

@implementation OperazioneViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.spesaLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"spesa"];
    self.puntiLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"punti"];
    
    NSString *numeroTessera = [[NSUserDefaults standardUserDefaults] objectForKey:@"numeroTessera"];
    
    NSMutableString *mu = [NSMutableString stringWithString:numeroTessera];
    [mu insertString:@" " atIndex:3];
    [mu insertString:@" " atIndex:7];
    [mu insertString:@" " atIndex:11];
    
    
    NSString * testo = [NSString stringWithFormat:@"ID CARD: %@",mu];
    
    
    self.numeroTesseraLabel.text = testo;
    self.puntiLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"punti"];

    
    
    float a = [[[NSUserDefaults standardUserDefaults] objectForKey:@"spesa"] floatValue];
    float b;
    
    if(a == 3){
        a = a / 10;
        b=0.1;
    }
    else{
        a = a / 10;
        b = a/2;
    }

   
    
    
    NSString *pSomma = [NSString stringWithFormat:@"%.1f",a];
    
    NSString *pDifferenza = [NSString stringWithFormat:@"%.1f",b];
    
    NSString *p10 = [NSString stringWithFormat:@"%.1f",b*2];
    
    NSString *p15 = [NSString stringWithFormat:@"%.1f",b*3];
    
    NSString *p20 = [NSString stringWithFormat:@"%.1f",b*4];
    
    [self.sommaButton setTitle:pSomma forState:UIControlStateNormal];
    
    [self.differenzaButton setTitle:pDifferenza forState:UIControlStateNormal];
    
    [self.meno10 setTitle:p10 forState:UIControlStateNormal];
    
    [self.meno15 setTitle:p15 forState:UIControlStateNormal];
    
    [self.meno20 setTitle:p20 forState:UIControlStateNormal];
    
    
    
    self.spesa = [self.spesaLabel.text floatValue];
    self.puntiSomma = a;
    self.puntiSottrazione = b;
    self.puntiSottrazione10 = b * 2;
    self.puntiSottrazione15 = b * 3;
    self.puntiSottrazione20 = b * 4;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sottrazione:(id)sender {
    
   
    
    NSString *tot = [NSString stringWithFormat:@"%.1f",[self.puntiLabel.text floatValue]];
    
  
    
    if([tot floatValue] - self.puntiSottrazione < 0){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                    message:@"punti non sufficienti" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        return;
    }
    
    NSString *numeroTessera = [[NSUserDefaults standardUserDefaults] objectForKey:@"numeroTessera"];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSString *jsonString = [[NSUserDefaults standardUserDefaults] objectForKey:@"json"];
    //NSString *jsonString =    @"{\"name\" : \"Dusty\", \"breed\": \"Poodle\", \"age\": \"7\"}";
    
    
    NSData* data_json = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    //data = [data subdataWithRange:NSMakeRange(0, [data length] - 1)];
    
    NSError *e;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data_json
                         
                                                        options:kNilOptions
                                                          error:&e];
    
    
    
    NSString* associato_id = [dic objectForKey:@"id"];
    
    NSString * sp = [NSString stringWithFormat:@"%.2f",self.spesa];
    
    NSString * puntiSottrazione = [NSString stringWithFormat:@"%.1f",self.puntiSottrazione];
    
    NSString * str = [NSString stringWithFormat:@"https://app.6local.it/associato/operazione"];
    

    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:str];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString * post = [NSString stringWithFormat:@"numeroTessera=%@&associato_id=%@&token=%@&spesa=%@&operazione=0&punti=%@", numeroTessera, associato_id,token,sp,puntiSottrazione];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    //[connection release];
    
    //start the connection
    [connection start];
    
    
}

- (IBAction)sottrazione10:(id)sender {
    NSString *tot = [NSString stringWithFormat:@"%.1f",[self.puntiLabel.text floatValue]];
    
    
    
    if([tot floatValue] - self.puntiSottrazione10 < 0){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                    message:@"punti non sufficienti" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        return;
    }
    
    NSString *numeroTessera = [[NSUserDefaults standardUserDefaults] objectForKey:@"numeroTessera"];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSString *jsonString = [[NSUserDefaults standardUserDefaults] objectForKey:@"json"];
    //NSString *jsonString =    @"{\"name\" : \"Dusty\", \"breed\": \"Poodle\", \"age\": \"7\"}";
    
    
    NSData* data_json = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    //data = [data subdataWithRange:NSMakeRange(0, [data length] - 1)];
    
    NSError *e;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data_json
                         
                                                        options:kNilOptions
                                                          error:&e];
    
    
    
    NSString* associato_id = [dic objectForKey:@"id"];
    
    NSString * sp = [NSString stringWithFormat:@"%.2f",self.spesa];
    
    NSString * puntiSottrazione = [NSString stringWithFormat:@"%.1f",self.puntiSottrazione10];
    
    NSString * str = [NSString stringWithFormat:@"https://app.6local.it/associato/operazione"];
    
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:str];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString * post = [NSString stringWithFormat:@"numeroTessera=%@&associato_id=%@&token=%@&spesa=%@&operazione=0&punti=%@", numeroTessera, associato_id,token,sp,puntiSottrazione];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    //[connection release];
    
    //start the connection
    [connection start];
}


- (IBAction)sottrazione15:(id)sender {
    NSString *tot = [NSString stringWithFormat:@"%.1f",[self.puntiLabel.text floatValue]];
    
    
    
    if([tot floatValue] - self.puntiSottrazione15 < 0){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                    message:@"punti non sufficienti" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        return;
    }
    
    NSString *numeroTessera = [[NSUserDefaults standardUserDefaults] objectForKey:@"numeroTessera"];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSString *jsonString = [[NSUserDefaults standardUserDefaults] objectForKey:@"json"];
    //NSString *jsonString =    @"{\"name\" : \"Dusty\", \"breed\": \"Poodle\", \"age\": \"7\"}";
    
    
    NSData* data_json = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    //data = [data subdataWithRange:NSMakeRange(0, [data length] - 1)];
    
    NSError *e;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data_json
                         
                                                        options:kNilOptions
                                                          error:&e];
    
    
    
    NSString* associato_id = [dic objectForKey:@"id"];
    
    NSString * sp = [NSString stringWithFormat:@"%.2f",self.spesa];
    
    NSString * puntiSottrazione = [NSString stringWithFormat:@"%.1f",self.puntiSottrazione15];
    
    NSString * str = [NSString stringWithFormat:@"https://app.6local.it/associato/operazione"];
    
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:str];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString * post = [NSString stringWithFormat:@"numeroTessera=%@&associato_id=%@&token=%@&spesa=%@&operazione=0&punti=%@", numeroTessera, associato_id,token,sp,puntiSottrazione];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    //[connection release];
    
    //start the connection
    [connection start];
}

- (IBAction)sottrazione20:(id)sender {
    NSString *tot = [NSString stringWithFormat:@"%.1f",[self.puntiLabel.text floatValue]];
    
    
    
    if([tot floatValue] - self.puntiSottrazione20 < 0){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                    message:@"punti non sufficienti" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        return;
    }
    
    NSString *numeroTessera = [[NSUserDefaults standardUserDefaults] objectForKey:@"numeroTessera"];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSString *jsonString = [[NSUserDefaults standardUserDefaults] objectForKey:@"json"];
    //NSString *jsonString =    @"{\"name\" : \"Dusty\", \"breed\": \"Poodle\", \"age\": \"7\"}";
    
    
    NSData* data_json = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    //data = [data subdataWithRange:NSMakeRange(0, [data length] - 1)];
    
    NSError *e;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data_json
                         
                                                        options:kNilOptions
                                                          error:&e];
    
    
    
    NSString* associato_id = [dic objectForKey:@"id"];
    
    NSString * sp = [NSString stringWithFormat:@"%.2f",self.spesa];
    
    NSString * puntiSottrazione = [NSString stringWithFormat:@"%.1f",self.puntiSottrazione20];
    
    NSString * str = [NSString stringWithFormat:@"https://app.6local.it/associato/operazione"];
    
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:str];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString * post = [NSString stringWithFormat:@"numeroTessera=%@&associato_id=%@&token=%@&spesa=%@&operazione=0&punti=%@", numeroTessera, associato_id,token,sp,puntiSottrazione];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    //[connection release];
    
    //start the connection
    [connection start];
}

- (IBAction)somma:(id)sender {
    

    NSString *numeroTessera = [[NSUserDefaults standardUserDefaults] objectForKey:@"numeroTessera"];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSString *jsonString = [[NSUserDefaults standardUserDefaults] objectForKey:@"json"];
    //NSString *jsonString =    @"{\"name\" : \"Dusty\", \"breed\": \"Poodle\", \"age\": \"7\"}";
    
    
    NSData* data_json = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    //data = [data subdataWithRange:NSMakeRange(0, [data length] - 1)];
    
    NSError *e;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data_json
                         
                                                        options:kNilOptions
                                                          error:&e];
    
    
    
    NSString* associato_id = [dic objectForKey:@"id"];
    
    NSString * sp = [NSString stringWithFormat:@"%.2f",self.spesa];
    
    NSString * puntiSottrazione = [NSString stringWithFormat:@"%.1f",self.puntiSomma];
    
    NSString * str = [NSString stringWithFormat:@"https://app.6local.it/associato/operazione"];
    
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:str];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString * post = [NSString stringWithFormat:@"numeroTessera=%@&associato_id=%@&token=%@&spesa=%@&operazione=1&punti=%@", numeroTessera, associato_id,token,sp,puntiSottrazione];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    //[connection release];
    
    //start the connection
    [connection start];

    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [self.receivedData appendData:data];
    
    //NSLog(@"%@", _responseData);
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    //NSLog(@"%@", _responseData.description);
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];

        
    if([htmlSTR  isEqual: @"1"] ){
       //operazione conclusa correttamente mando ad home
        UIStoryboard *sb;
        NSString *deviceType = [UIDevice currentDevice].model;
        
        
        if([deviceType isEqualToString:@"iPhone"])
            sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        
        UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"TotViewController"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vc animated:YES completion:NULL];
    }
        
    
    
    
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                message:@"Problemi di connessione" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [mes show];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
