//
//  SpesaViewController.h
//  6local
//
//  Created by GLOCAL on 12/06/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpesaViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *conferma;
@property (weak, nonatomic) IBOutlet UILabel *numeroTessera;
@property (weak, nonatomic) IBOutlet UITextField *spesaTextField;
@property (weak, nonatomic) IBOutlet UILabel *totalePuntiCard;
@property (retain, nonatomic) NSMutableData *receivedData;
@property (retain, nonatomic) NSURLConnection *connection;
@property (weak, nonatomic) IBOutlet UITextField *confermaBack;
@end

@interface NSString (usefull_stuff)
- (BOOL) isAllDigits;
@end

