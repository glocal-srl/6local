//
//  OperazioneViewController.h
//  6local
//
//  Created by GLOCAL on 18/06/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OperazioneViewController : UIViewController
@property (retain, nonatomic) NSMutableData *receivedData;
@property (retain, nonatomic) NSURLConnection *connection;
@property (weak, nonatomic) IBOutlet UILabel *spesaLabel;
@property (weak, nonatomic) IBOutlet UILabel *puntiLabel;
@property (weak, nonatomic) IBOutlet UIButton *sommaButton;

@property (weak, nonatomic) IBOutlet UIButton *meno10;
@property (weak, nonatomic) IBOutlet UIButton *meno15;
@property (weak, nonatomic) IBOutlet UIButton *meno20;
@property (weak, nonatomic) IBOutlet UILabel *numeroTesseraLabel;
@property (weak, nonatomic) IBOutlet UIButton *differenzaButton;

@property (nonatomic, assign) float spesa;
@property (nonatomic, assign) float puntiSomma;
@property (nonatomic, assign) float puntiSottrazione;
@property (nonatomic, assign) float puntiSottrazione10;
@property (nonatomic, assign) float puntiSottrazione15;
@property (nonatomic, assign) float puntiSottrazione20;
@property (nonatomic, assign) float totalePunti;



@end
