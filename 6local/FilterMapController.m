//
//  FilterMapController.m
//  6Local
//
//  Created by pierangelo on 20/05/15.
//  Copyright (c) 2015 Glocal srl. All rights reserved.
//

#import "FilterMapController.h"
#import "FilterDetailController.h"
///#import "PointDetailController.h"


@interface FilterMapController ()
{
    NSMutableArray *myObject;
    // A dictionary object
    NSDictionary *dictionary;
    // Define keys
    NSString *denominazione;
    NSString *categoria;
    NSString *comune;
    NSString *provincia;
    NSString *indirizzo;
    NSString *civico;
    NSString *telefono;
    NSString *email;
    NSString *sito;
    NSString *latitude;
    NSString *longitude;
    NSString *distanza;
}
@end

@implementation FilterMapController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    denominazione = @"denominazione";
    categoria = @"categoria";
    comune = @"comune";
    provincia =@"provincia";
    indirizzo= @"indirizzo";
    civico = @"civico";
    telefono = @"telefono";
    email = @"email";
    sito = @"sito";
    latitude = @"latitudine";
    longitude = @"longitudine";
    distanza = @"distanza";
    
    myObject = [[NSMutableArray alloc] init];
    
    self.tableData.hidden = true;

    
    
    NSString *fonte2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"ricerca"];
    NSString *valorericerca = [[NSUserDefaults standardUserDefaults] objectForKey:@"valore"];


/*    if([fonte2 isEqualToString:@"true"])
        url = [NSString stringWithFormat:@"https://app.6local.it/json/search/alimentari"]; */
        NSString * url = [NSString stringWithFormat:@"http://app.6local.it/json/search/"];
    
    ///if([fonte2 isEqualToString:@"true"])
        ///url = [NSString stringWithFormat:@"http://app.6local.it/json/searc/alimentari/"];
    
    if([fonte2 isEqualToString:@"true"]){
        self.tableData.hidden=false;
        url = [NSString stringWithFormat:@"http://app.6local.it/json/search/%@",valorericerca];
    }



    
    NSData *jsonSource = [NSData dataWithContentsOfURL:
                          [NSURL URLWithString:url]];
    
    id jsonObjects = [NSJSONSerialization JSONObjectWithData:
                      jsonSource options:NSJSONReadingMutableContainers error:nil];
    
    for (NSDictionary *dataDict in jsonObjects) {
        NSString *denominazione_data = [dataDict valueForKeyPath:@"Associato.denominazione"];
        NSString *categoria_data = [dataDict valueForKeyPath:@"Associato.categoria"];
        NSString *comune_data = [dataDict valueForKeyPath:@"Associato.comune"];
        NSString *provincia_data = [dataDict valueForKeyPath:@"Associato.provincia"];
        NSString *indirizzo_data = [dataDict valueForKeyPath:@"Associato.indirizzo"];
        NSString *civico_data = [dataDict valueForKeyPath:@"Associato.civico"];
        NSString *telef_data = [dataDict valueForKeyPath:@"Associato.tel"];
        NSString *posta_data = [dataDict valueForKeyPath:@"Associato.email"];
        NSString *sito_data = [dataDict valueForKeyPath:@"Associato.web"];
        NSString *latitudine_data = [dataDict valueForKeyPath:@"Associato.latitudine"];
        NSString *longitudine_data = [dataDict valueForKeyPath:@"Associato.longitudine"];
        NSString *distanza_data = [dataDict valueForKeyPath:@"0.distance"];

        
        
        NSLog(@"DENOMINAZIONE: %@",denominazione_data);
        NSLog(@"CATEGORIA: %@", categoria_data);
        NSLog(@"CITTA: %@", comune_data);
        //NSLog(@"INDIRIZZO: @", indirizzo_data);
        NSLog(@"LATITUDINE %@", latitudine_data);
        NSLog(@"LONGITUDINE %@", longitudine_data);
        NSLog(@"DISTANZA %@", distanza_data);

        
        
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                      denominazione_data, denominazione,
                      categoria_data, categoria,
                      comune_data, comune,
                      provincia_data, provincia,
                      indirizzo_data, indirizzo,
                      civico_data, civico,
                      telef_data, telefono,
                      posta_data, email,
                      sito_data, sito,
                      latitudine_data, latitude,
                      longitudine_data, longitude,
                      distanza_data, distanza,
                      nil];
        [myObject addObject:dictionary];
    }
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"filterpointSegue"]) {
        NSIndexPath *indexPath = [self.tableData indexPathForSelectedRow];
        FilterDetailController *dv = segue.destinationViewController;
        dv.myObject = [myObject objectAtIndex:indexPath.row];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return myObject.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Item";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell=[[UITableViewCell alloc]initWithStyle:
              UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    
    NSDictionary *tmpDict = [myObject objectAtIndex:indexPath.row];
    
    NSMutableString *denominazio;
    denominazio = [NSMutableString stringWithFormat:@"%@",
                   [tmpDict objectForKeyedSubscript:denominazione]];
    
    
    /// calcolo distanza
    
    /// a startLati e startLongi andrebbero passate coordinate da GPS
    /// mentre endLat e endLong ricevono da dal json.
    NSString *startLati = @"45.59418";
    NSString *startLongi = @"10.03713";
    
    NSString *endLat = [NSMutableString stringWithFormat:@"%@",
                        [tmpDict objectForKeyedSubscript:latitude]];
    NSString *endLong = [NSMutableString stringWithFormat:@"%@",
                         [tmpDict objectForKeyedSubscript:longitude]];
    
    /// Funzione CLLocationDistance calcolo distanza tra due punti
    CLLocation *startLocation = [[CLLocation alloc] initWithLatitude:[startLati doubleValue]longitude:[startLongi doubleValue]];
    CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:[endLat doubleValue] longitude:[endLong doubleValue]];
    CLLocationDistance distance = [startLocation distanceFromLocation:endLocation] / 1000;
    
    NSMutableString *dista;
    dista = [NSMutableString stringWithFormat:@"%f Km.", distance];
    

    
    UIImage *img = [UIImage imageNamed:@"location.png"];
    
    NSMutableString *catego;
    catego = [NSMutableString stringWithFormat:@"%@",
                   [tmpDict objectForKeyedSubscript:categoria]];
    
    cell.textLabel.text = denominazio;
    cell.detailTextLabel.text= catego;
    cell.imageView.frame = CGRectMake(0, 0, 80, 70);
    cell.imageView.image =img;
    //cell.imageView.image = [UIImage imageNamed:@"location.png"];
    
    return cell;
}
- (IBAction)ricerca:(id)sender {
    NSString *ricerca = _Ricercatore.text;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *var1 = @"true";
    [defaults setObject:var1 forKey:@"ricerca"];
    [defaults setObject:ricerca forKey:@"valore"];

    [defaults synchronize];
    
    UIViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"filterMapNav"];
    home.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:home animated:YES completion:nil];
    
    /*UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"PointViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL]; */

}


- (IBAction)backHome:(id)sender {
    UIViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"InizioView"];
    home.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:home animated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
