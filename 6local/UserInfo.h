//
//  UserInfo.h
//  6local
//
//  Created by GLOCAL on 27/05/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface UserInfo : NSObject

@property (nonatomic) BOOL tipo;
@property (nonatomic) NSString * username;
@property (nonatomic) NSString * password;

+ (UserInfo*) sharedInstance;


@end
