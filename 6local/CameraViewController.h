//
//  CameraViewController.h
//  6local
//
//  Created by GLOCAL on 11/06/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface CameraViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewPreview;
@property (weak, nonatomic) IBOutlet UILabel *qrCode;

@end
