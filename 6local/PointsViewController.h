//
//  PointsViewController.h
//  6Local
//
//  Created by pierangelo on 20/05/15.
//  Copyright (c) 2015 Glocal srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface PointsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>


@property (strong, nonatomic) IBOutlet UITableView *tableData;

@end
