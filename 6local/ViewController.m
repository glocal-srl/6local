//
//  ViewController.m
//  6local
//
//  Created by GLOCAL on 27/05/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//


#import "ViewController.h"
#import "CameraViewController.h"
#import "InfoViewController.h"
#import "UserInfo.h"
#import "LocationsViewController.h"

@interface ViewController ()

@end

@implementation NSString (Random)

+ (NSString *)randomAlphanumericStringWithLength:(NSInteger)length
{
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity:length];
    
    for (int i = 0; i < length; i++) {
        [randomString appendFormat:@"%C", [letters characterAtIndex:arc4random() % [letters length]]];
    }
    
    return randomString;
}

@end

@implementation ViewController

@synthesize userNameTextField;
@synthesize passwordTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //NSLog(@"%@", [UserInfo sharedInstance].username);
    
    //[UserInfo sharedInstance].username = [NSString stringWithFormat:@"%@%@", @"Hello", [UserInfo sharedInstance].username];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//tentativo di login, faccio richiesta al server
- (IBAction)loginClick:(id)sender {
    
   

    //NSLog(@"%@", [UserInfo sharedInstance].username);
    
    if ([userNameTextField.text isEqualToString:@""] || [passwordTextField.text isEqualToString:@""] ) {
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                    message:@"Devi inserire username e password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        return;
        //[mes release];
    }
    
    self.loginButton.enabled = NO;
    
    //initialize new mutable data
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    //[data release];
    
    NSString * token = [NSString randomAlphanumericStringWithLength:30];
    
    //salvo il token
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:token forKey:@"token"];
    [defaults synchronize];
    
    
    
    NSString * str = [NSString stringWithFormat:@"https://app.6local.it/associato/login"];
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:str];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    NSString * post = [NSString stringWithFormat:@"username=%@&password=%@&token=%@", userNameTextField.text, passwordTextField.text,token];
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    //[connection release];
    
    //start the connection
    [connection start];
    
}


#pragma mark NSURLConnection Delegate Methods



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it

}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [self.receivedData appendData:data];
    
    //NSLog(@"%@", _responseData);
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    //NSLog(@"%@", _responseData.description);
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
    if([htmlSTR  isEqual: @"0"] ){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                    message:@"Username o password errate" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        self.loginButton.enabled = YES;
        [mes show];
    }
    else
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:htmlSTR forKey:@"json"];
        [defaults synchronize];
        
        NSData* data = [htmlSTR dataUsingEncoding:NSUTF8StringEncoding];
        //data = [data subdataWithRange:NSMakeRange(0, [data length] - 1)];
        
        NSError *e;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data
                                                            options:kNilOptions
                                                              error:&e];
        
        NSString* tipo;
        tipo = [dic objectForKey:@"tipo"];
       
        
        
        
        //apro schermata relativa
        UIStoryboard *sb;
        NSString *deviceType = [UIDevice currentDevice].model;
        
        if([deviceType isEqualToString:@"iPhone"])
            sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        if([tipo  isEqual: @"associato"]){
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
            vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:vc animated:YES completion:NULL];
        }else{
            UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"TesseratoViewController"];
            vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
            [self presentViewController:vc animated:YES completion:NULL];
        }
    }
    
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                message:@"Problemi di connessione" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [mes show];
    self.loginButton.enabled = YES;
}
- (IBAction)funzionamento:(id)sender {
    //apro schermata relativa
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"info"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)info:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:info@6local.it"]];
    
  
    
    
}


- (IBAction)golocation:(id)sender {
    UIViewController *mov = [self.storyboard instantiateViewControllerWithIdentifier:@"navigazione"];
    mov.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:mov animated:YES completion:nil];
}

- (IBAction)backHome:(id)sender {
    UIViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"InizioView"];
    home.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:home animated:YES completion:nil];
}


@end
