//
//  TotViewController.m
//  6local
//
//  Created by GLOCAL on 24/06/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import "TotViewController.h"

@interface TotViewController ()

@end

@implementation TotViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *numeroTessera = [[NSUserDefaults standardUserDefaults] objectForKey:@"numeroTessera"];
    
    NSMutableString *mu = [NSMutableString stringWithString:numeroTessera];
    [mu insertString:@" " atIndex:3];
    [mu insertString:@" " atIndex:7];
    [mu insertString:@" " atIndex:11];
    
    NSString * testo = [NSString stringWithFormat:@"ID CARD: %@",mu];
    

    _numeroTessera.text = testo;
    
    
    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSString *jsonString = [[NSUserDefaults standardUserDefaults] objectForKey:@"json"];
    //NSString *jsonString =    @"{\"name\" : \"Dusty\", \"breed\": \"Poodle\", \"age\": \"7\"}";
    
    
    NSData* data_json = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    //data = [data subdataWithRange:NSMakeRange(0, [data length] - 1)];
    
    NSError *e;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data_json
                         
                                                        options:kNilOptions
                                                          error:&e];
    
    
    
    NSString* associato_id = [dic objectForKey:@"id"];
    
    NSString * sp = @"1235345345";
    
    NSString * str = [NSString stringWithFormat:@"http://app.6local.it/tessera/punti/%@/%@/1/%@",numeroTessera,associato_id,sp];
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:str];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    [connection start];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)apriLettore:(id)sender {
    
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"CameraViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [self.receivedData appendData:data];
    
  
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    //NSLog(@"%@", _responseData.description);
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
    //NSLog(@"%@" , htmlSTR);
    
    
    self.punti.text = htmlSTR;
    

    
    
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                message:@"Problemi di connessione" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [mes show];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
