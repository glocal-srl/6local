//
//  AppDelegate.h
//  6local
//
//  Created by GLOCAL on 27/05/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfo.h"

@interface NSString (Random)

- (NSString *)randomAlphanumericStringWithLength:(NSInteger)length;

@end

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

