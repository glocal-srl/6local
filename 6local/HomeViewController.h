//
//  HomeViewController.h
//  6local
//
//  Created by GLOCAL on 12/06/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *infoUtenteLabel;

@end
