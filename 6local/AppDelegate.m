//
//  AppDelegate.m
//  6local
//
//  Created by GLOCAL on 27/05/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "TesseratoViewController.h"

@implementation AppDelegate



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    //controllo se l'utente si era già loggato
    NSString *json = [[NSUserDefaults standardUserDefaults] objectForKey:@"json"];
    
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if(json == nil)
        NSLog(@"non loggato");
    else{
        
        NSData* data = [json dataUsingEncoding:NSUTF8StringEncoding];
        //data = [data subdataWithRange:NSMakeRange(0, [data length] - 1)];
        
        NSError *e;
        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data
                                                            options:kNilOptions
                                                              error:&e];
        
        NSString* tipo;
       
        tipo = [dic objectForKey:@"tipo"];
       
        
        if([deviceType isEqualToString:@"iPhone"])
            sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        
        
        if([tipo  isEqual: @"associato"]){
           HomeViewController *homeViewController = (HomeViewController *)[sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
            self.window.rootViewController = homeViewController;
        }else{
            TesseratoViewController *tesseratoViewController = (TesseratoViewController *)[sb instantiateViewControllerWithIdentifier:@"TesseratoViewController"];
            self.window.rootViewController = tesseratoViewController;
        }
        
        
    }
    
    
    


    //NSLog(@"%@", json);
    //[UserInfo sharedInstance].username = [NSString stringWithFormat: @"eccola"];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

