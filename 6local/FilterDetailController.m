//  6Local
//
//  Created by pierangelo on 20/05/15.
//  Copyright (c) 2015 Glocal srl. All rights reserved.
//

#import "FilterDetailController.h"
#import "MapAnnotation.h"


@interface FilterDetailController ()

@end

@implementation FilterDetailController

@synthesize mapView;


@synthesize myObject;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *address = [myObject valueForKey:@"indirizzo"];
    NSString *civico = [myObject valueForKey:@"civico"];
    NSString *comun = [myObject valueForKey:@"comune"];
    NSString *provi = [myObject valueForKey:@"provincia"];
    self.denominazioneLabel.text = [myObject valueForKey:@"denominazione"];
    self.categoriaLabel.text = [myObject valueForKey:@"categoria"];
    self.comuneLabel.text = [NSString stringWithFormat:@"%@ (%@)", comun, provi];
    self.indirizzoLabel.text = [NSString stringWithFormat:@"%@ %@", address, civico];
    self.telefonoLabel.text = [myObject valueForKey:@"telefono"];
    self.emailLabel.text = [myObject valueForKey:@"email"];
    self.webLabel.text = [myObject valueForKey:@"sito"];

    
    // (180 * M_PI) / 180 == M_PI, so just use M_PI
    self.apri.hidden = true;
    self.apri.transform = CGAffineTransformMakeRotation(M_PI);
    self.filetto.hidden = true;
    
    //self.mapView.delegate = self;
    
    NSString *latitude = [myObject valueForKey:@"latitudine"];
    NSString *longitude = [myObject valueForKey:@"longitudine"];
    
    NSString *lati = [NSString stringWithFormat:@"%@", latitude];
    NSString *longi = [NSString stringWithFormat:@"%@", longitude];
    
    
    
    NSLog(@"LATITUDINE %@", lati);
    NSLog(@"LONGITUDINE %@", longi);
    
    MapAnnotation *mapPoint = [[MapAnnotation alloc] init];
    mapPoint.coordinate = CLLocationCoordinate2DMake([lati doubleValue], [longi doubleValue]);
    //mapPoint.title = self.location.title;
    
    // Add it to the map view
    [self.mapView addAnnotation:mapPoint];
    
    // Zoom to a region around the pin
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(mapPoint.coordinate, 500, 500);
    [self.mapView setRegion:region];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)apriTendina:(id)sender {
    self.filetto.hidden = true;
    self.apri.hidden = true;
    self.chiudi.hidden = false;
    self.filettodue.hidden = false;
    mapView.frame = CGRectMake(0, 180, 320, 504);
}


- (IBAction)chiudi:(id)sender {
    self.apri.hidden = false;
    self.chiudi.hidden = true;
    self.filetto.hidden = false;
    self.filettodue.hidden = true;
    mapView.frame = CGRectMake(0, 54, 320, 504);
    
}

- (IBAction)percorso:(id)sender {
    NSString *latitudin = [myObject valueForKey:@"latitudine"];
    NSString *longitudin = [myObject valueForKey:@"longitudine"];
    ///NSString *urlString = @"http://maps.apple.com/maps?daddr=45.6724207,9.9694798";
    NSString *urlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%@,%@",latitudin,longitudin];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}


- (IBAction)linkWeb:(id)sender {
    NSString *mysito = [myObject valueForKey:@"sito"];
    NSLog(@"Vai: %@",mysito);
    NSString *urlweb = [NSString stringWithFormat:@"http://%@",mysito];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlweb]];
}

- (IBAction)linkmail:(id)sender {
    NSString *mymail = [myObject valueForKey:@"email"];
    NSString *urlmail = [NSString stringWithFormat:@"mailto:%@",mymail];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlmail]];
}

- (IBAction)linkTel:(id)sender {
    NSString *mytel = [myObject valueForKey:@"telefono"];
    NSString *urltel = [NSString stringWithFormat:@"Tel:%@",mytel];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urltel]];
}


/*
 #pragma mark - Navigation
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

