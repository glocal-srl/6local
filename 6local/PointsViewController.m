//
//  PointsViewController.m
//  6Local
//
//  Created by pierangelo on 20/05/15.
//  Copyright (c) 2015 Glocal srl. All rights reserved.
//

#import "PointsViewController.h"
#import "PointDetailController.h"


@interface PointsViewController ()
{
    NSMutableArray *myObject;
    // A dictionary object
    NSDictionary *dictionary;
    // Define keys
    NSString *denominazione;
    NSString *categoria;
    NSString *comune;
    NSString *provincia;
    NSString *indirizzo;
    NSString *civico;
    NSString *telefono;
    NSString *email;
    NSString *sito;
    NSString *latitude;
    NSString *longitude;
    NSString *distanza;

}
@end

@implementation PointsViewController

CLLocationManager *locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    denominazione = @"denominazione";
    categoria = @"categoria";
    comune = @"comune";
    provincia =@"provincia";
    indirizzo= @"indirizzo";
    civico = @"civico";
    telefono = @"telefono";
    email = @"email";
    sito = @"sito";
    latitude = @"latitudine";
    longitude = @"longitudine";
    distanza = @"distanza";
    
   
    
    NSLog(@"geoloc");
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    
    NSLog(@"richiesta coordinate");
    
   
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueDettaglio"]) {
        NSIndexPath *indexPath = [self.tableData indexPathForSelectedRow];
        PointDetailController *dv = segue.destinationViewController;
        dv.myObject = [myObject objectAtIndex:indexPath.row];
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return myObject.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Item";
    
    UILabel *temp = nil;
    temp = [[UILabel alloc] initWithFrame:CGRectMake(50, 35, 300, 50)];
    temp.textColor = [UIColor colorWithRed:0.098f green:0.623f blue:0.87f alpha:1.f];
    [temp setFont:[UIFont fontWithName:@"Arial" size:15]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell=[[UITableViewCell alloc]initWithStyle:
              UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
    }

    
    
    NSDictionary *tmpDict = [myObject objectAtIndex:indexPath.row];
    
    NSMutableString *denominazio;
    denominazio = [NSMutableString stringWithFormat:@"%@",
                   [tmpDict objectForKeyedSubscript:denominazione]];
    
    NSMutableString *cat;
    cat = [NSMutableString stringWithFormat:@"%@",
                   [tmpDict objectForKeyedSubscript:categoria]];

    
    NSMutableString *dista;
    dista = [NSMutableString stringWithFormat:@"%@ Km.",
             [tmpDict objectForKey:distanza]];
    
    /*NSMutableString *indi;
     indi = [NSMutableString stringWithFormat:@"%@",
     [tmpDict objectForKey:indirizzo]]; */
    
    /*NSMutableString *images;
     images = [NSMutableString stringWithFormat:@"%@ ",
     [tmpDict objectForKey:thumbnail]]; */
    
    //NSURL *url = [NSURL URLWithString:[tmpDict objectForKey:thumbnail]];
    //NSData *data = [NSData dataWithContentsOfURL:url];
    //UIImage *img = [[UIImage alloc]initWithData:data];
    UIImage *img = [UIImage imageNamed:@"location.png"];
    
    
    cell.textLabel.text = denominazio;
    cell.detailTextLabel.text= dista;
    cell.imageView.frame = CGRectMake(0, 0, 80, 70);
    cell.imageView.image =img;
    [cell.contentView addSubview:temp];
    temp.text = cat;


    //cell.imageView.image = [UIImage imageNamed:@"location.png"];
    
    return cell;
}


- (IBAction)backHome:(id)sender {
    UIViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"InizioView"];
    home.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:home animated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        
        denominazione = @"denominazione";
        categoria = @"categoria";
        comune = @"comune";
        provincia =@"provincia";
        indirizzo= @"indirizzo";
        civico = @"civico";
        telefono = @"telefono";
        email = @"email";
        sito = @"sito";
        latitude = @"latitudine";
        longitude = @"longitudine";
        distanza = @"distanza";
        NSString *lat = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        NSString *lon = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        
        NSString *stringa = [NSString stringWithFormat:@"http://app.6local.it/json/near/%@/%@/",lat,lon];
        
        myObject = [[NSMutableArray alloc] init];
        NSData *jsonSource = [NSData dataWithContentsOfURL:
                              [NSURL URLWithString:stringa]];
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:
                          jsonSource options:NSJSONReadingMutableContainers error:nil];
        
        for (NSDictionary *dataDict in jsonObjects) {
            NSString *denominazione_data = [dataDict valueForKeyPath:@"Associato.denominazione"];
            NSString *categoria_data = [dataDict valueForKeyPath:@"Associato.categoria"];
            NSString *comune_data = [dataDict valueForKeyPath:@"Associato.comune"];
            NSString *provincia_data = [dataDict valueForKeyPath:@"Associato.provincia"];
            NSString *indirizzo_data = [dataDict valueForKeyPath:@"Associato.indirizzo"];
            NSString *civico_data = [dataDict valueForKeyPath:@"Associato.civico"];
            NSString *telef_data = [dataDict valueForKeyPath:@"Associato.tel"];
            NSString *posta_data = [dataDict valueForKeyPath:@"Associato.email"];
            NSString *sito_data = [dataDict valueForKeyPath:@"Associato.web"];
            NSString *latitudine_data = [dataDict valueForKeyPath:@"Associato.latitudine"];
            NSString *longitudine_data = [dataDict valueForKeyPath:@"Associato.longitudine"];
            NSString *distanza_data = [dataDict valueForKeyPath:@"0.distance"];
            
            
            
            
            
            
            
            dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                          denominazione_data, denominazione,
                          categoria_data, categoria,
                          comune_data, comune,
                          provincia_data, provincia,
                          indirizzo_data, indirizzo,
                          civico_data, civico,
                          telef_data, telefono,
                          posta_data, email,
                          sito_data, sito,
                          latitudine_data, latitude,
                          longitudine_data, longitude,
                          distanza_data, distanza,
                          nil];
            [myObject addObject:dictionary];
            
            [self.tableData reloadData];
            
            
            
        }
    }
}

@end
