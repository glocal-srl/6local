//
//  InizioViewController.m
//  6Local
//
//  Created by pierangelo on 20/05/15.
//  Copyright (c) 2015 Glocal srl. All rights reserved.
//

#import "InizioViewController.h"
#import "InfoViewController.h"
#import "ViewController.h"
#import "PointsViewController.h"
#import "Location.h"


@interface InizioViewController ()

@end

@implementation InizioViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *var1 = @"false";
    [defaults setObject:var1 forKey:@"ricerca"];
    [defaults synchronize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)linkTesserato:(id)sender {
    //apro schermata relativa
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];

    UIViewController *mov = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginTesserato"];
    mov.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:mov animated:YES completion:nil];
}


- (IBAction)linkAffiliato:(id)sender {
    //apro schermata relativa
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];

    UIViewController *mov = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    mov.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:mov animated:YES completion:nil];
}


- (IBAction)linkMappa:(id)sender {
    //apro schermata relativa
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];

    UIViewController *mov = [self.storyboard instantiateViewControllerWithIdentifier:@"navigazione"];
    mov.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:mov animated:YES completion:nil];
}


- (IBAction)cercaLink:(id)sender {
    //apro schermata relativa
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];

    UIViewController *mov = [self.storyboard instantiateViewControllerWithIdentifier:@"filterMapNav"];
    mov.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:mov animated:YES completion:nil];
}


- (IBAction)linkInfo:(id)sender {
    //apro schermata relativa
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];

    UIViewController *mov = [self.storyboard instantiateViewControllerWithIdentifier:@"info"];
    mov.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:mov animated:YES completion:nil];
}


- (IBAction)linkFunzionamento:(id)sender {
    //apro schermata relativa
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    UIViewController *mov = [self.storyboard instantiateViewControllerWithIdentifier:@"info"];
    mov.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:mov animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
