//
//  SpesaViewController.m
//  6local
//
//  Created by GLOCAL on 12/06/14.
//  Copyright (c) 2014 Glocal srl. All rights reserved.
//

#import "SpesaViewController.h"

@interface SpesaViewController ()

@end


@implementation SpesaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *numeroTessera = [[NSUserDefaults standardUserDefaults] objectForKey:@"numeroTessera"];
    
    NSMutableString *mu = [NSMutableString stringWithString:numeroTessera];
    [mu insertString:@" " atIndex:3];
    [mu insertString:@" " atIndex:7];
    [mu insertString:@" " atIndex:11];
    
    
    NSString * testo = [NSString stringWithFormat:@"ID CARD:  %@",mu];
    
    
    
    _numeroTessera.text = testo;
    
    
    

    NSMutableData *data = [[NSMutableData alloc] init];
    self.receivedData = data;
    
    NSString *jsonString = [[NSUserDefaults standardUserDefaults] objectForKey:@"json"];
    //NSString *jsonString =    @"{\"name\" : \"Dusty\", \"breed\": \"Poodle\", \"age\": \"7\"}";
    
    
    NSData* data_json = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    //data = [data subdataWithRange:NSMakeRange(0, [data length] - 1)];
    
    NSError *e;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data_json
                         
                                                        options:kNilOptions
                                                          error:&e];
    
    
    
    NSString* associato_id = [dic objectForKey:@"id"];
    
    
    NSString * str = [NSString stringWithFormat:@"http://app.6local.it/tessera/punti/%@/%@/1/",numeroTessera,associato_id];
    
    
    //initialize url that is going to be fetched.
    NSURL *url = [NSURL URLWithString:str];
    
    //initialize a request from url
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    //initialize a connection from request
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.connection = connection;
    //[connection release];
    self.conferma.enabled = NO;
    //start the connecti
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)confermaSpesa:(id)sender {
    //mando alla schermata per la selezione dell'operazione

    if ([self.spesaTextField.text isEqualToString:@""]) {
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                    message:@"Devi inserire il valore spesa" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        return;
        //[mes release];
    }
    
    NSDecimal decimalValue;
    NSString * testo = [NSString stringWithFormat:@"%@",self.spesaTextField.text];
    testo = [testo stringByReplacingOccurrencesOfString:@"," withString:@"."];
    
    NSScanner *sc = [NSScanner scannerWithString:testo];
    [sc scanDecimal:&decimalValue];
    BOOL isDecimal = [sc isAtEnd];
    
    if (!isDecimal){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                    message:@"Valore spesa non valido" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        return;
    }
    
    if([self.spesaTextField.text floatValue] < 0){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                    message:@"Valore spesa non valido" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        return;
    }
    
    if([self.spesaTextField.text floatValue] > 15000){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                    message:@"Valore spesa non computabile" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        return;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:testo forKey:@"spesa"];
    [defaults synchronize];
    
    
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"OperazioneViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [self.receivedData appendData:data];
    
    
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    //NSLog(@"%@", _responseData.description);
    //initialize convert the received data to string with UTF8 encoding
    NSString *htmlSTR = [[NSString alloc] initWithData:self.receivedData
                                              encoding:NSUTF8StringEncoding];
    //NSLog(@"%@" , htmlSTR);
    
    if([htmlSTR  isEqual: @"NO REG"]){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"ATTIVAZIONE"
                                                    message:@"Attiva 6local card dal sito \nwww.6local.it" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        UIStoryboard *sb;
        NSString *deviceType = [UIDevice currentDevice].model;
        
        
        if([deviceType isEqualToString:@"iPhone"])
            sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        
        UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vc animated:YES completion:NULL];
        return;
    }
    else
    if([htmlSTR  isEqual: @"NO ACT"]){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"CONFERMA ATTIVAZIONE"
                                                    message:@"Per completare la procedura di attivazione card, rispondi alla mail che ti abbiamo inviato" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        UIStoryboard *sb;
        NSString *deviceType = [UIDevice currentDevice].model;
        
        
        if([deviceType isEqualToString:@"iPhone"])
            sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        
        UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vc animated:YES completion:NULL];
        return;
    }
    /*
    if([htmlSTR  isEqual: @"NO ACT"]){
        UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"CONFERMA ATTIVAZIONE"
                                                    message:@"Conferma mail di attivazione" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [mes show];
        UIStoryboard *sb;
        NSString *deviceType = [UIDevice currentDevice].model;
        
        
        if([deviceType isEqualToString:@"iPhone"])
            sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        else
            sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
        
        UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"HomeViewController"];
        vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:vc animated:YES completion:NULL];
        return;
    }
    */
    self.totalePuntiCard.text = htmlSTR;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:htmlSTR forKey:@"punti"];
    [defaults synchronize];
    self.conferma.enabled = YES;
    
    
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    UIAlertView* mes=[[UIAlertView alloc] initWithTitle:@"Attenzione"
                                                message:@"Problemi di connessione" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [mes show];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)annulla:(id)sender {
    
    UIStoryboard *sb;
    NSString *deviceType = [UIDevice currentDevice].model;
    
    
    if([deviceType isEqualToString:@"iPhone"])
        sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    else
        sb = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"CameraViewController"];
    vc.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:vc animated:YES completion:NULL];
    
}


@end


@implementation NSString (usefull_stuff)

- (BOOL) isAllDigits
{
    NSCharacterSet* nonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    NSRange r = [self rangeOfCharacterFromSet: nonNumbers];
    return r.location == NSNotFound;
}
@end

